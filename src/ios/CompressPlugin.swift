import Foundation
import Photos

@objc(CompressPlugin) class CompressPlugin : CDVPlugin {

  func compress(command: CDVInvokedUrlCommand) {
    let input = command.arguments[0] as? String ?? "";

    var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAsString: "ERROR OCCURED");
    
    let qualityOfServiceClass = QOS_CLASS_BACKGROUND
    let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
    
    dispatch_async(backgroundQueue, {
        // do some task
        let url = NSURL(fileURLWithPath: input)
        let outputURL = self.tempCompressFilePath("mp4")
        
        print(url, "INPUT: ", input)
        
        self.compressVideo(url, outputURL: outputURL, outputFileType: AVFileTypeMPEG4, handler: { session in
            
            print("compressVideo IN")
            
            if let currSession = session {
                
                if currSession.status == .Completed {
                    
                    print("Session Completed")
                    
                    if let data = NSData(contentsOfURL: currSession.outputURL!) {

                        if(data.length > 0) {
                            
                            print("Data length > 0")
                            
                            let compressedVideoPath = currSession.outputURL!.absoluteString   

                            pluginResult = CDVPluginResult(
                                status: CDVCommandStatus_OK,
                                messageAsString: compressedVideoPath
                            )
                        }
                    }
                } else {
                    print("Session Not Completed")
                }
            }

            self.commandDelegate!.sendPluginResult(
                pluginResult, 
                callbackId: command.callbackId
            )                
        })  
    }); 
  }

  func compressVideo(inputURL: NSURL, outputURL: NSURL, outputFileType:String, handler:(session: AVAssetExportSession?)-> Void)
  {
      let urlAsset = AVURLAsset(URL: inputURL, options: nil)
      
      let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality)
      
      exportSession?.outputURL = outputURL
      
      exportSession?.outputFileType = outputFileType
      
      exportSession?.shouldOptimizeForNetworkUse = true
      
      exportSession?.exportAsynchronouslyWithCompletionHandler { () -> Void in
          
          handler(session: exportSession)
      }
  }
  
  func tempCompressFilePath(ext: String) -> NSURL {
      
      return getPath("tempCompressMovie", ext: ext)
  }
  
  func getPath(name: String, ext: String) -> NSURL{
      
      let urlPath  = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent(name).URLByAppendingPathExtension(ext)
      let tempPath = urlPath.absoluteString
      
      if NSFileManager.defaultManager().fileExistsAtPath(urlPath.path!) {
          do {
              try NSFileManager.defaultManager().removeItemAtURL(urlPath)
          }
          catch let error as NSError {
              
              print("Failed to remove item \(tempPath), error = \(error)")
          }
      }
      
      return NSURL(string: tempPath)!
  }
  
  func getFileSize(fileURL : NSURL) -> Int {
      if let data = NSData(contentsOfURL: fileURL) {
          
          return data.length / 1024
      }
      
      print("Error while calculating file size.")
      
      return 0
  }

}
