var exec = require('cordova/exec');

exports.compress = function(arg0, success, error) {
    exec(success, error, "Compress", "compress", [arg0]);
};